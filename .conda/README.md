# Publish to conda

There is two way of publishing to conda:
- A fully automatic in CI that publish to an "openfisca" channel. See below for more information.
- A more complex for Conda-Forge.

We use both for openfisca-core but only _openfisca channel_ for this package.

## Automatic upload

The CI automaticaly upload the PyPi package, see the `.gitlab-ci.yml`, step `deploy_conda`.

## Manual actions made to make it works the first time

- Create an account on https://anaconda.org.
- Create a token on https://anaconda.org/leximpact/settings/access with _Allow write access to the API site_. Warning, it expire on 2023/01/13.
- Put the token in a CI env variable ANACONDA_TOKEN.

## CI config

Nous n'utilisons plus Conda.

```yaml
deploy_conda:
  stage: anaconda
  before_script:
   - ''
  cache: []
  image: continuumio/miniconda3
  script:
    - conda install -y conda-build anaconda-client toml
    - python3 deploy/get_pypi_info.py -p leximpact-socio-fisca-simu-etat
    - conda config --set anaconda_upload yes
    - conda build -c conda-forge -c Leximpact -c openfisca --token $ANACONDA_TOKEN --user Leximpact .conda
  only:
    - master
```

## Manual actions before CI exist

To create the package you can do the following in the project root folder:

- Edit `.conda/meta.yaml` and update it if needed:
    - Version number
    - Hash SHA256
    - Package URL on PyPi

- Build & Upload package:
    - `conda install -c anaconda conda-build anaconda-client`
    - `conda config --add channels conda-forge`
    - `conda config --set channel_priority strict`
    - `conda build .conda`
    - `anaconda login`
    - `anaconda upload package-name-<VERSION>-py_0.tar.bz2`