############
### Makefile for NBDev (leximpact_socio_fisca_simu_etat)
############

# to exit at first error, remove .ONESHELL:

SHELL := /bin/bash
SRC = $(wildcard notebooks/*.ipynb)

all: leximpact-socio-fiscal-simu-etat docs

package_version := $(shell grep "^version" pyproject.toml)

leximpact-socio-fiscal-simu-etat: $(SRC)
	-$(MAKE) lib
	touch leximpact-socio-fiscal-simu-etat

install:
	poetry install -E api
	poetry run python -m ipykernel install --name leximpact-socio-fiscal-simu-etat-kernel --user
	poetry run pre-commit install

api:
	# poetry run api
	poetry run uvicorn leximpact_socio_fisca_simu_etat_api.server:app --reload

precommit:
	-poetry run pre-commit run --all-files
	-$(MAKE) lib
	-poetry run pre-commit run --all-files
	-echo "XXXXXXXX Last check XXXXXXXXXXX"
	-poetry run pre-commit run --all-files

lib:
	rm leximpact_socio_fisca_simu_etat/*.py | true
	@echo $(package_version)
	sed -i "/^version/c\$(package_version)" settings.ini
	poetry run nbdev_export
	sed -i 's/from .schema import ReformeSocioFiscale/from leximpact_socio_fisca_simu_etat.schema import ReformeSocioFiscale/g' leximpact_socio_fisca_simu_etat/run_simulation_dump.py

docs_serve: docs
	cd docs && bundle exec jekyll serve

docs: $(SRC)
	poetry run nbdev_docs
	touch docs

test:
	poetry run pytest
	# poetry run nbdev_test_nbs
	# poetry run pytest -s tests/test_z_ratios_calage_prod.py -s tests/test_jwt.py -k test_check_error_token_malformed

test-nb:
	mkdir -p papermill
	poetry run papermill --execution-timeout 300 notebooks/schema.ipynb ./papermill/schema-paper.ipynb
	poetry run papermill --execution-timeout 300 notebooks/status.ipynb ./papermill/status.ipynb
	poetry run papermill --execution-timeout 300 notebooks/simu_budget_survey_scenario.ipynb ./papermill/simu_budget_survey_scenario.ipynb

release: pypi conda_release
	poetry run nbdev_bump_version

conda_release:
	poetry run fastrelease_conda_package

pypi: dist
	poetry run twine upload --repository pypi dist/*

dist: clean
	poetry run python setup.py sdist bdist_wheel

clean:
	rm -rf dist
	rm -rf dump_survey_scenario*
	rm -rf leximpact_socio_fisca_simu_etat.egg-info
	rm -rf papermill

bump_patch:
	poetry version patch
	$(MAKE) precommit

bump_minor:
	poetry version minor
	$(MAKE) precommit

bump_major:
	poetry version major
	$(MAKE) precommit
