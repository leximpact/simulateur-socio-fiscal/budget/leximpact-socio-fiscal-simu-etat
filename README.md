# French State Budget Simulation API

<!-- WARNING: THIS FILE WAS AUTOGENERATED! DO NOT EDIT! -->

*Unofficial HTTP API for OpenFisca*

Used by [LexImpact](https://leximpact.an.fr/), a simulator of the French
tax-benefit system.

Make use of [OpenFisca](https://openfisca.org/en/) a rules as code tax
benefit system.

L’API de calcul fait appel à différents éléments : - OpenFisca pour
réaliser le calcul, à travers
[leximpact-survey-scenarios](https://git.leximpact.dev/leximpact/leximpact-survey-scenario). -
Le projet de réforme du PLF ou PLFSS que l’on recupère
d’[openfisca-france-reforms](https://git.leximpact.dev/openfisca/openfisca-france-reforms) -
La réforme de l’utilisateur qu’il saisit sur le site web - Le fichier
INSEE ERFS-FPR transformé, qui vient de
[leximpact-prepare-data](https://git.leximpact.dev/leximpact/leximpact-prepare-data). -
Une brique de sécurité pour vérifier que l’appel provient bien d’une
personne autorisée. - Un cache qui mémorise dynamiquement le résultat
des calculs pour ne pas les refaire inutilement.

## Install

`pip install leximpact_socio_fisca_simu_etat`

## How to use

- Import the package
- Define your reform
- Compute the result of your reform

``` python
from leximpact_socio_fisca_simu_etat.schema import ReformeSocioFiscale
from leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario import (
    compute_all_simulation,
)
```

    iaidrdi has been updated in leximpact-survey-scenario
    plus_values_prelevement_forfaitaire_unique_ir has been updated in leximpact-survey-scenario
    rfr_plus_values_hors_rni has been updated in leximpact-survey-scenario
    rpns_imposables has been updated in leximpact-survey-scenario
    rpns_autres_revenus has been updated in leximpact-survey-scenario

``` python
reform = ReformeSocioFiscale(
    base=2024,
    amendement={
        "prelevements_sociaux.contributions_sociales.csg.activite.imposable.taux": 0.0,
    },
    output_variables=["csg_imposable_salaire"],
    quantile_nb=0,
)
resultat = await compute_all_simulation(reform)
```

    [leximpact_socio-fisca-simu-etat DEBUG @ 10:56:13] Il faut créer un nouveau survey_scenario ☹
    [leximpact_socio-fisca-simu-etat INFO @ 10:56:13] 🔥 XXXXXXXX MODE_PRE-PLF XXXXXXXX 🔥
    [leximpact_socio-fisca-simu-etat DEBUG @ 10:56:15] create_survey_scenario - start
    [leximpact_socio-fisca-simu-etat DEBUG @ 10:56:15] create_survey_scenario - dump_directory exists
    [leximpact_socio-fisca-simu-etat DEBUG @ 10:56:20] create_survey_scenario - done
    [leximpact_socio-fisca-simu-etat DEBUG @ 10:56:20] Calcul de variable='csg_imposable_salaire_foyer_fiscal'
    [leximpact_socio-fisca-simu-etat DEBUG @ 10:56:22] compute_reform - Temps de traitement pour une simulation 8.33286492001207 secondes.
    [leximpact_socio-fisca-simu-etat DEBUG @ 10:56:22] Temps de traitement total pour la simulation 8.334454773008474 secondes. Annee TBS 2024

    LeximpactErfsSurveyScenario : Using /mnt/data-out/leximpact-socio-fiscal-simu-etat/integ as config_dirpath
    years_available=[2021] vs years=[2024]
    WARNING: no data for 2024, will took 2021
    Données du scénario : 
     {'input_data_table_by_entity_by_period': {2024: {'individu': 'individu_2021', 'famille': 'famille_2021', 'foyer_fiscal': 'foyer_fiscal_2021', 'menage': 'menage_2021'}}, 'survey': 'leximpact_2021'}

``` python
print(
    f"Montant total de la csg : {resultat.result['amendement'].state_budget['csg_imposable_salaire']:,} €"
)
```

    Montant total de la csg : 0.0 €

## Fonctionnement de l’API et des workers

Pour faire face aux problèmes de temps de réponses et surcharge mémoire,
nous avons mis en place un système de worker asynchrone :

- L’API écrit sa demande dans une file d’attente de Redis.
- Un ou plusieurs workers écoutent cette file d’attente et font les
  calculs. L’avantage c’est qu’ils gardent l’objet Survey-scenario en
  mémoire sans risque de mélanger les requêtes.
- Le résultat est écrit dans Redis.
- L’API lit le résultat dans Redis et le retourne à l’utilisateur. Ou
  une erreur si pas de réponse après 2 minutes.

Voir la [MR
62](https://git.leximpact.dev/leximpact/leximpact-socio-fiscal-simu-etat/-/merge_requests/62)
pour plus de détails.

Voici un schéma du fonctionnement :

``` mermaid
sequenceDiagram
    Front-&gt;&gt;+API Simu-Etat: Ask for simulation
    Worker--&gt;&gt;Worker: Waiting...
    API Simu-Etat-&gt;&gt;+Worker: Ask for simulation
    API Simu-Etat--&gt;&gt;API Simu-Etat: Waiting...
    alt Result
       Worker-&gt;&gt;-API Simu-Etat: Simulation result
       API Simu-Etat-&gt;&gt;Front: Simulation result
    else Timeout
       API Simu-Etat-&gt;&gt;Front: TimeOut !
    end
```

Ce système permet au worker de conserver en mémoire un objet
`survey_scenario` complètement initialisé, cela accélère les calculs. De
plus nous pouvons décider du nombre de worker en fonction de la mémoire
disponible.

Les workers intègre une gestion des demandes pour mettre en veille des
workers supplémentaires qui ne servent que pendant les pics de charge.

- Permet d’affecter un numéro aux workers.
- Libération de la mémoire si plus de simulation depuis 30 minutes pour
  les workers dont le numéro est \> 2.
- Le status de l’API passera en erreur si plus de 6 simulations en
  attentes. (donc alerte UptimeRobot)
- Les simulations non commencées demandées depuis plus de 60 secondes
  sont annulées.
- Les résultats non reçus après 80 secondes sont abandonnés.

Voir la [MR
64](https://git.leximpact.dev/leximpact/leximpact-socio-fiscal-simu-etat/-/merge_requests/64)
pour plus de détails.

Voici un diagramme du fonctionnement logique d’un worker :

``` mermaid
flowchart TD
    A[get_message] --&gt; C{Message ?}
    C --&gt;|No| D{worker_id &gt; 2 \nand\nlast_simulation &gt; 30 minutes ?}
    D --&gt;|No| A
    D --&gt;|Yes| G[Go sleeping]
    G --&gt; A
    C --&gt;|Yes| wid{worker_id &gt; 2}
    wid --&gt; |No| simu[Do simulation]
    wid --&gt;|Yes| E{En veille et trop de mess ?}
    E --&gt;|Yes| wakeup[Wake Up]
    E --&gt;|No| A
    wakeup --&gt; simu
    simu --&gt; A
```

C’est dans le script de démarrage *Systemd* du worker que l’on défini
son numéro.

# How to develop

Please see notebooks/contributing.ipynb
