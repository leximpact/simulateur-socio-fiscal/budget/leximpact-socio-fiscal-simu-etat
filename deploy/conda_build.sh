#!/bin/bash
conda install -y conda-build anaconda-client toml
python3 deploy/get_pypi_info.py -p leximpact_socio_fisca_simu_etat
# conda config --set anaconda_upload yes
conda build -c conda-forge -c leximpact -c openfisca .conda
