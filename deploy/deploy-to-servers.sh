#!/bin/bash
###############################################################################
# Script qui permet de déployer l'application    
# sur les serveurs de production et d'intégration
# Usage : bash deploy/deploy-to-servers.sh --ssh_private_key "$SSH_PRIVATE_KEY" --servers_ip "163.172.173.191" --ci_project_name "leximpact-socio-fiscal-simu-etat" --ci_commit_ref_name "40-tester-le-mode-plf-conrefactuel" --ci_commit_sha "83c1e01d57754a49ef7e0e0acc706c3cc9ba8823"
###############################################################################
set -e
# Gestion des paramètre de la ligne de commande
while [[ $# -gt 0 ]]; do
  case $1 in
    --ssh_private_key)
      SSH_PRIVATE_KEY="$2"
      shift # past argument
      shift # past value
      ;;
    --bastion_ip)
      MY_BASTION_IP="$2"
      shift
      shift
      ;;
    --servers_ip)
      SERVERS_IP="$2"
      shift
      shift
      ;;
    --ci_project_name)
      CI_PROJECT_NAME="$2"
      shift
      shift
      ;;
    --ci_commit_ref_name)
      CI_COMMIT_REF_NAME="$2"
      shift
      shift
      ;;
    --ci_commit_sha)
      CI_COMMIT_SHA="$2"
      shift
      shift
      ;;
    --worker_only)
      WORKER_ONLY="$2"
      shift
      shift
      ;;
    *)
      echo "Unknown option: $1"
      exit 1
      ;;
  esac
done

eval $(ssh-agent -s)
ssh-add <(echo "$SSH_PRIVATE_KEY")

if [ -f /.dockerenv ]; then
    mkdir -p ~/.ssh
    echo -e "Host *\n\tStrictHostKeyChecking no\n\tLogLevel quiet\n" > ~/.ssh/config
fi;
echo "Deploying to servers list: $SERVERS_IP"
for SERVER_IP in $SERVERS_IP
do
    echo "Deploying to $SERVER_IP"
    if [ -n "$MY_BASTION_IP" ]
    then
        echo "scp -o \"ProxyCommand ssh gitlabci@$MY_BASTION_IP nc -w 1 %h 22\" deploy/deploy.sh leximpact@$SERVER_IP:$CI_PROJECT_NAME/deploy/deploy.sh"
        scp -o "ProxyCommand ssh gitlabci@$MY_BASTION_IP nc -w 1 %h 22" deploy/deploy.sh leximpact@$SERVER_IP:$CI_PROJECT_NAME/deploy/deploy.sh
        echo "ssh -J gitlabci@$MY_BASTION_IP leximpact@$SERVER_IP \"cd $CI_PROJECT_NAME && bash deploy/deploy.sh $CI_COMMIT_REF_NAME $CI_COMMIT_SHA $WORKER_ONLY && exit\""
        ssh -J gitlabci@$MY_BASTION_IP leximpact@$SERVER_IP "cd $CI_PROJECT_NAME && bash deploy/deploy.sh $CI_COMMIT_REF_NAME $CI_COMMIT_SHA $WORKER_ONLY && exit"
    else
        echo "scp deploy/deploy.sh leximpact@$SERVER_IP:$CI_PROJECT_NAME/deploy/deploy.sh"
        scp deploy/deploy.sh leximpact@$SERVER_IP:$CI_PROJECT_NAME/deploy/deploy.sh
        echo "ssh leximpact@$SERVER_IP \"cd $CI_PROJECT_NAME && bash deploy/deploy.sh $CI_COMMIT_REF_NAME $CI_COMMIT_SHA $WORKER_ONLY && exit\""
        ssh leximpact@$SERVER_IP "cd $CI_PROJECT_NAME && bash deploy/deploy.sh $CI_COMMIT_REF_NAME $CI_COMMIT_SHA $WORKER_ONLY && exit"
    fi
    echo "Deployed successfully to $SERVER_IP"
    echo "-----------------------------------"
done
echo "Deployed successfully to all servers"
exit 0
