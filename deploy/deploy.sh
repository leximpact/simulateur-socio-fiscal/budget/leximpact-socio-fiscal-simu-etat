#!/bin/bash
# Stop at first error
set -e

# Read .env file
[ -f .env ] && export $(grep -v '^#' .env | xargs)

CI_COMMIT_REF_NAME=$1
CI_COMMIT_SHA=$2
WORKER_ONLY=$3
if [ -z "$CI_COMMIT_REF_NAME" ]
then
      echo "CI_COMMIT_REF_NAME is empty !"
      exit 1
fi
if [ -z "$CI_COMMIT_SHA" ]
then
      echo "CI_COMMIT_SHA is empty !"
      exit 1
fi


if [ -z "$WORKER_ONLY" ]
then
  echo "WORKER_ONLY is empty !"
else
  echo "WORKER_ONLY=$WORKER_ONLY"
  if [ "$WORKER_ONLY" = "YES" ]; then
      echo "WORKER_ONLY is set to YES"
      if grep -q "^REDIS_QUEUE_NAME=prod-plf$" .env; then
        if systemctl --user list-units | grep leximpact-socio-fiscal-simu-api.service
        then
          echo "On est sur l'API de prod PLF => On ne fait rien !"
          exit 0
        fi
      fi
  else
      echo "WORKER_ONLY is not set to YES"
      # Add alternative commands here
  fi
fi

echo "Starting deploy for branch $CI_COMMIT_REF_NAME at commit $CI_COMMIT_SHA"
echo "Fetching from Gitlab..."
git fetch --all
echo "Reset git env..."
git reset --hard origin/$CI_COMMIT_REF_NAME
echo "Checkout..."
git checkout -B $CI_COMMIT_REF_NAME $CI_COMMIT_SHA
echo "Pull..."
git pull origin $CI_COMMIT_REF_NAME
echo "Stop all services..."


if [ -f /home/leximpact/.config/systemd/user/leximpact-socio-fiscal-simu-api.service ]
then
  echo "Arrêt des services sur un conteneur Proxmox"
  systemctl --user stop leximpact-socio-fiscal-simu-api.service
  systemctl --user stop leximpact-socio-fiscal-simu-worker-1.service | true
  systemctl --user stop leximpact-socio-fiscal-simu-worker-2.service | true
  systemctl --user stop leximpact-socio-fiscal-simu-worker-3.service | true
  systemctl --user stop leximpact-socio-fiscal-simu-worker-4.service | true
else
  echo "Arrêt du worker Scaleway"
  sudo systemctl stop redis-worker-scaleway.service
fi

echo "Install deps..."
rm -rf .venv
rm -rf $DUMP_PATH
# Check if any files/directories matching the pattern still exist
if ls $DUMP_PATH 1> /dev/null 2>&1; then
  echo "Failed to remove some files or directories."
  exit 1
else
  echo "All files and directories removed successfully."
fi
/home/leximpact/.local/bin/poetry config virtualenvs.in-project true
/home/leximpact/.local/bin/poetry install --extras api
# Free space
rm -rf /home/leximpact/.cache/*

source .venv/bin/activate
sed -i 's/from .schema import ReformeSocioFiscale/from leximpact_socio_fisca_simu_etat.schema import ReformeSocioFiscale/g' leximpact_socio_fisca_simu_etat/run_simulation_dump.py
if systemctl --user list-units | grep leximpact-socio-fiscal-simu-api.service
then
  echo "Dedibox : besoin de dump ?"
  if grep -q "^REDIS_QUEUE_NAME=prod-plf$" "$ENV_FILE"; then
      echo "REDIS_QUEUE_NAME=prod-plf found in $ENV_FILE"
      python leximpact_socio_fisca_simu_etat/run_simulation_dump.py
      if ls $DUMP_PATH/baseline/rfr/2025.npy 1> /dev/null 2>&1; then
        echo "Dump $DUMP_PATH/baseline/rfr/2025.npy created successfully."
      else
        echo "No dump $DUMP_PATH/baseline/rfr/2025.npy created !"
        exit 1
      fi
  else
      echo "REDIS_QUEUE_NAME=prod-plf not found in $ENV_FILE"
  fi
else
  echo "Calcul des dumps sur Scaleway"
  python leximpact_socio_fisca_simu_etat/run_simulation_dump.py
  if ls $DUMP_PATH/baseline/rfr/2025.npy 1> /dev/null 2>&1; then
    echo "Dump $DUMP_PATH/baseline/rfr/2025.npy created successfully."
  else
    echo "No dump $DUMP_PATH/baseline/rfr/2025.npy created !"
    exit 1
  fi
fi
deactivate

echo "Restart service..."

if [ -f /home/leximpact/.config/systemd/user/leximpact-socio-fiscal-simu-api.service ]
then
  echo "Lancement des services sur un serveur Dedibox"
  systemctl --user start leximpact-socio-fiscal-simu-api.service
  systemctl --user start leximpact-socio-fiscal-simu-worker-1.service | true
  systemctl --user start leximpact-socio-fiscal-simu-worker-2.service | true
  systemctl --user start leximpact-socio-fiscal-simu-worker-3.service | true
  systemctl --user start leximpact-socio-fiscal-simu-worker-4.service | true
else
  echo "Lancement du worker Scaleway"
  sudo systemctl start redis-worker-scaleway.service
fi




echo "Deploy done"
