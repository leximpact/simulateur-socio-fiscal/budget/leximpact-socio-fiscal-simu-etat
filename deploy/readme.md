# Utilisation de Docker pour LexImpact Simu-Etat

## Construction de l'image


Depuis la racine du projet `leximpact-socio-fiscal-simu-etat`:

```bash
docker build -t leximpact/socio-fiscal-simu-etat:0.0.1 -f deploy/Dockerfile-ci .
```

## Instancier l'image et obtenir un shell dans le conteneur

`docker run -it -v $PWD/data-out/:/mnt/data-out/ leximpact/socio-fiscal-simu-etat:0.0.1 bash`

## Publication de l'image

```bash
docker push leximpact/socio-fiscal-simu-etat:0.0.1
```
