ratios_calage_baseline = {
    "allegement_cotisation_allocations_familiales": 0.9331682081126549,
    "allegement_cotisation_maladie": 0.9839931110980418,
    "allegement_general": 0.930064652289416,
    "csg_imposable_salaire": 1.0426234057584973,
    "csg_deductible_salaire": 1.0426234057584973,
    "csg_imposable_retraite": 1.1364044054031053,
    "csg_deductible_retraite": 1.1364044054031053,
    "vieillesse_deplafonnee_salarie": 1.0149116630783983,
    "vieillesse_plafonnee_salarie": 1.0149116630783983,
    "vieillesse_deplafonnee_employeur": 1.0149116630783983,
    "vieillesse_plafonnee_employeur": 1.0149116630783983,
    "famille": 0.9654026542781146,
    "mmid_employeur": 0.9753900301077353,
    "agirc_arrco_employeur": 0.9380062375293967,
    "agirc_arrco_salarie": 0.9380062375293967,
    "contribution_equilibre_general_employeur": 0.9380062375293967,
    "contribution_equilibre_general_salarie": 0.9380062375293967,
    "contribution_solidarite_autonomie": 1.3190272887920271,
    "fnal_contribution": 0.7033178680028783,
    "chomage_employeur": 0.9655428705898448,
    "irpp_economique": 0.9902064015867293,
    "af": 1.0589900569644124,
    "cf": 1.1842364572625088,
    "ars": 1.1545386736284713,
    "paje_base": 0.9956922313955541,
    "paje_naissance": 1.3522749164225083,
}
