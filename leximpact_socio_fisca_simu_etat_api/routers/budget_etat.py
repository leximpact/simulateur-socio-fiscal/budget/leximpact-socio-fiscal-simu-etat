import asyncio
import json
import time
import uuid
from contextlib import asynccontextmanager
from random import random
from typing import Optional

from anyio import create_task_group
from fastapi import APIRouter, Depends, Header, HTTPException, Request, Response
from leximpact_common_python_libraries.cache import Cache
from leximpact_common_python_libraries.config import Configuration
from leximpact_common_python_libraries.logger import Logger
from starlette.status import HTTP_401_UNAUTHORIZED  # , HTTP_500_INTERNAL_SERVER_ERROR

from leximpact_socio_fisca_simu_etat.schema import (
    AllSimulationResult,
    OneSimulationResult,
    ReformeSocioFiscale,
)

# from leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario import (
#     compute_all_simulation,
# )
from leximpact_socio_fisca_simu_etat_api.security import get_token_header

"""
As we run task asynchronously, we need to store the result somewhere.
A production solution may be to use something like Redis.
"""
temporary_returns_storage = {}

logger = Logger(package_name="leximpact-socio-fiscal-simu-etat").logger
config = Configuration(project_folder="leximpact-socio-fiscal-simu-etat")
router = APIRouter()
cache = Cache(project_folder="leximpact-socio-fiscal-simu-etat")
REDIS_QUEUE_NAME = config.get("REDIS_QUEUE_NAME", fail_on_missing=False, default="dev")
WORKER_TIMEOUT = int(config.get("WORKER_TIMEOUT", fail_on_missing=False, default=120))


async def wait_for_disconnect(request: Request) -> True:
    """
    Read the request stream until the client disconnects
    Returns True if the client disconnects
    """
    _receive = request._receive
    while (await _receive())["type"] != "http.disconnect":
        pass
    logger.debug("wait_for_disconnect - Client disconnected, cancelling task.")
    return True


@asynccontextmanager
async def create_request_task_group(request: Request):
    """
    Create a task group that can be cancelled
    1. We create a task group that will be cancelled if the client disconnects
    2. We create a task that will wait for a disconnect event
    3. We await the task group and the task concurrently
    4. When the client disconnects, the task group is cancelled and the task that was waiting for the event will be cancelled too
    5. The CancelledError will be caught in the outer task group, which will then cancel the inner task group
    6. The inner task group will cancel all its tasks
    """

    async def cancel_on_disconnect():
        await wait_for_disconnect(request)
        logger.debug(
            "create_request_task_group - Client disconnected, raising CancelledError"
        )
        raise asyncio.CancelledError()

    async with create_task_group() as outer_tg:
        outer_tg.start_soon(cancel_on_disconnect)

        async with create_task_group() as tg:
            yield tg

        outer_tg.cancel_scope.cancel()


def get_random_result():
    return OneSimulationResult(
        state_budget={"csg": random() * 1e9},
        quantiles=[
            {
                "fraction": float((i + 1) / 10),
                "csg": random() * 1_000 * -(10**i),
            }
            for i in range(10)
        ],
    )


@router.post("/state_simulation_random", response_model=AllSimulationResult)
async def reform_fake(reform: ReformeSocioFiscale):
    logger.info("Appel de /state_simulation_random")
    montant_csg_etat = AllSimulationResult(
        result={
            "base": get_random_result(),
            "plf": get_random_result(),
            "amendement": get_random_result(),
        },
        errors=["WARNING : FAKE DATA"],
    )
    logger.info("Retour de /state_simulation_random avec succès")
    return montant_csg_etat


def call_compute_all_simulation_sync(task_id: str, reform: ReformeSocioFiscale) -> None:
    global temporary_returns_storage
    logger.debug(f"call_compute_all_simulation_sync for {task_id=} start")
    # temporary_returns_storage[task_id] = compute_all_simulation(reform)
    temporary_returns_storage[task_id] = AllSimulationResult(
        result={
            "base": get_random_result(),
            "plf": get_random_result(),
            "amendement": get_random_result(),
        },
        errors=["WARNING : FAKE DATA"],
    )
    for i in range(5):
        print("Working...", i)
        time.sleep(1)
    logger.debug(
        f"call_compute_all_simulation_sync for {task_id=} stop : {temporary_returns_storage[task_id]=}"
    )


async def call_compute_all_simulation_async(
    task_id: str, reform: ReformeSocioFiscale
) -> None:
    global temporary_returns_storage
    logger.debug(f"API - call_compute_all_simulation_async for {task_id=} start")

    msg = {"jobid": task_id, "reform": reform.json()}

    cache.cache.rpush(REDIS_QUEUE_NAME, json.dumps(msg))
    cache.cache.expire(REDIS_QUEUE_NAME, 60)

    finished = False
    start = time.perf_counter()
    result = None
    while not finished:
        await asyncio.sleep(0.05)
        result = cache.get(task_id)
        if result is not None:
            cache.cache.delete(task_id)
            finished = True
        if time.perf_counter() - start > WORKER_TIMEOUT:
            logger.warning(
                f"API - call_compute_all_simulation_async - {task_id=} Timeout !"
            )
            finished = True
    if result:
        simu_out = json.loads(result)
        simu_out = AllSimulationResult.parse_obj(simu_out)
        logger.info(
            f"API - call_compute_all_simulation_async - Calcul {task_id=} recu par redis en {time.perf_counter() - start:.2f} secondes"
        )
    else:
        simu_out = AllSimulationResult(
            result={},
            errors=["Le calcul a pris trop de temps à se faire."],
        )
        logger.warning(
            f"API - call_compute_all_simulation_async - Le calcul {task_id=} a pris trop de temps à se faire."
        )
    temporary_returns_storage[task_id] = simu_out
    #             return AllSimulationResult.parse_obj(simu_out)

    # temporary_returns_storage[task_id] = await compute_all_simulation(reform)
    # temporary_returns_storage[task_id] = AllSimulationResult(
    #     result={
    #         "base": get_random_result(),
    #         "plf": get_random_result(),
    #         "amendement": get_random_result(),
    #     },
    #     errors=["WARNING : FAKE DATA",
    # )
    # for i in range(5):
    #     print("Working...", i)
    #     await asyncio.sleep(0.01)
    #     time.sleep(1)
    logger.debug(f"API - call_compute_all_simulation_async for {task_id=} stop")


async def call_compute_all_simulation(
    task_id: str, reform: ReformeSocioFiscale
) -> None:
    # logger.debug(f"call_compute_all_simulation for {task_id=} start")
    await call_compute_all_simulation_async(task_id, reform)
    # return await asyncio.to_thread(call_compute_all_simulation_sync, task_id, reform)


@router.post(
    "/default_state_simulation",
    response_model=AllSimulationResult,
)
async def default_state_simulation(
    request: Request,
    response: Response,
    reform: ReformeSocioFiscale,
    request_id: Optional[str] = Header(None),
):
    """
    This route does not require authentication.
    It is for displaying the budget page to the public.
    """
    logger.info("Appel de /default_state_simulation")
    # Remove amendement to avoid someone to exploit the API
    reform.amendement = None
    task_id = str(uuid.uuid4())
    async with create_request_task_group(request) as tg:
        tg.start_soon(call_compute_all_simulation, task_id, reform)
    montant_budget_etat = temporary_returns_storage.get(task_id)
    if montant_budget_etat:
        del temporary_returns_storage[task_id]
    else:
        logger.warning(f"default_state_simulation - {task_id=} killed")
        raise HTTPException(
            status_code=499,  # client closed request
            detail=f"Getting result for {task_id=} failed",
        )
    if request_id:
        response.headers["Request-Id"] = request_id

    return montant_budget_etat


@router.post(
    "/state_simulation",
    response_model=AllSimulationResult,
    dependencies=[Depends(get_token_header)],
)
async def state_simulation(
    request: Request,
    response: Response,
    reform: ReformeSocioFiscale,
    request_id: Optional[str] = Header(None),
):
    logger.info(f"Appel de /state_simulation pour {request_id=}")
    # Generate a unique id for the task
    # We can't use the request_id for security reason
    task_id = str(uuid.uuid4())
    async with create_request_task_group(request) as tg:
        tg.start_soon(call_compute_all_simulation, task_id, reform)
    montant_budget_etat = temporary_returns_storage.get(task_id)
    if montant_budget_etat:
        del temporary_returns_storage[task_id]
    else:
        logger.warning(f"default_state_simulation - {task_id=} killed")
        raise HTTPException(
            status_code=499,  # client closed request
            detail=f"Getting result for {task_id=} failed",
        )
    if request_id:
        response.headers["Request-Id"] = request_id
    # TODO: Remettre sécurité !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # if check_aggregates(reform, montant_budget_etat):
    #     logger.info("Retour de /state_simulation avec succès")
    #     return montant_budget_etat
    # else:
    #     raise HTTPException(
    #         status_code=HTTP_500_INTERNAL_SERVER_ERROR,
    #         detail="Aggregate did not match computed value for base.",
    #     )
    return montant_budget_etat


@router.get("/clear_cache")
async def clear_cache_db(secret: str):
    if secret == config.get("ADMIN_PASSWORD"):
        return {"cache_cleared": cache.clear_cache()}
    else:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="Invalid password",
        )
