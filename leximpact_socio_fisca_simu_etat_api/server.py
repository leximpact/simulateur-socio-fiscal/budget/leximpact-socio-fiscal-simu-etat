import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from leximpact_common_python_libraries.cache import Cache
from prometheus_fastapi_instrumentator import Instrumentator

from leximpact_socio_fisca_simu_etat.schema import Status
from leximpact_socio_fisca_simu_etat.status import get_status
from leximpact_socio_fisca_simu_etat_api.routers import budget_etat

cache = Cache(project_folder="leximpact-socio-fiscal-simu-etat")
app = FastAPI()
app.include_router(budget_etat.router)


@app.on_event("startup")
async def startup():
    Instrumentator().instrument(app).expose(app)


# CORS
# Needed for Javascript Browser
# TODO: add origins in the config file
origins = [
    "https://leximpact.an.fr",
    "http://localhost:3000",
    "http://localhost:5173",
    "http://localhost:8000",
    "http://localhost",
    "https://budget.leximpact.an.fr",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_origin_regex=r"https://.*.leximpact.dev",
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# To allow *
# app.add_middleware(
#     CORSMiddleware,
#     allow_origins=["*"],
#     allow_credentials=False,  # Can't be True when allow_origins is set to ["*"].
#     allow_methods=["*"],
#     allow_headers=["*"],
# )


@app.get("/", tags=["root"])
def root():
    return {"message": "please go to /docs"}


# 'head' sert à éviter les erreurs 405 avec UptimeRobot
@app.head("/status", tags=["root"], response_model=Status)
@app.get("/status", tags=["root"], response_model=Status)
async def status():
    return await get_status()


def start_dev():
    """
    Launched with `poetry run start` at root level
    Only for DEV, don't use for production
    """
    uvicorn.run(
        "leximpact_socio_fisca_simu_etat_api.server:app",
        host="0.0.0.0",
        port=8000,
        reload=True,
    )
