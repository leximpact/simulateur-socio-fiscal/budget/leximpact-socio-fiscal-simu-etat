# Analyse des temps de calcul du budget

Sandra avait fait des tests avec Snakeviz pour analyser l'usage de la mémoire : https://git.leximpact.dev/leximpact/simulateur-socio-fiscal/budget/leximpact-socio-fiscal-simu-etat/-/tree/improve-survey-scenario-memory-leak/tests/memory

Il y a des infos intéressantes dans [tests/load_testing/README.md](tests/load_testing/README.md)

Et le script [tests/load_testing/profiling_simu-etat.py](tests/load_testing/profiling_simu-etat.py) permet de profiler le code. Le fichier résultant peut être visualisé avec https://github.com/nschloe/tuna


## Utilisation d'Austin pour les temps de calcul

Installer l'outil Austin : `poetry run pip install austin-dist`

Installer l'[extension pour visualiser les résultats](https://marketplace.visualstudio.com/items?itemName=p403n1x87.austin-vscode)

Lancer un test avec Austin : `poetry run austin -so profile_test_af_web.austin python -m pytest -vv  tests/test_af_web.py`

Charger le fichier `.austin` dans l'onglet `FLAME GRAPH` (F1 -> _Load Austin sample..._) pour visualiser les résultats :

![alt text](images/austin-usage.png)

Le chargement est long (< 5 min) car le fichier est gros : 2 Go pour un seul test !
Le script [tests/test_af_web.py](tests/test_af_web.py) est un test qui utilise un fichier JSON qui est un copier-coller de l'appel du front au 17/09/2024 sur un amendement des  [tests/amendement_af_17-09-2024.json](tests/amendement_af_17-09-2024.json).

Ce que l'on constate c'est que le calcul des quantiles semble refaire les calculs déjà fait pour les agrégâts :

![alt text](images/image-0.png)

```json
    "output_variables": [
        "rfr_par_part",
        "allegement_cotisation_allocations_familiales",
        "famille"
    ],
    "quantile_base_variable": [
        "rfr_par_part"
    ],
    "quantile_compare_variables": [
        "allegement_cotisation_allocations_familiales",
        "famille",
        "rfr"
    ],
```

Si on retire `rfr` de `quantile_compare_variables`, la liste des quantiles à calculer, on gagne 40% de temps de calcul (on passe de 56s à 35s pour le test) :
```json
    "output_variables": [
        "rfr_par_part",
        "allegement_cotisation_allocations_familiales",
        "famille"
    ],
    "quantile_base_variable": [
        "rfr_par_part"
    ],
    "quantile_compare_variables": [
        "allegement_cotisation_allocations_familiales",
        "famille",
    ],
```
![alt text](images/image-1.png)

Si on ajoute `rfr` dans "output_variables" pour qu'il soit calculé avant les quantiles, on revient sur le temps de calcul initial (58 s)

```json
    "output_variables": [
        "rfr_par_part",
        "allegement_cotisation_allocations_familiales",
        "famille",
        "rfr"
    ],
    "quantile_base_variable": [
        "rfr_par_part"
    ],
    "quantile_compare_variables": [
        "allegement_cotisation_allocations_familiales",
        "famille",
        "rfr"
    ],
```

Et le temps de calcul des quantiles est de nouveau long alors que toutes les variables sont censées être déjà calculées 🤯 :

![alt text](images/image-2.png)

En réactivant les dumps, on passe de 47s à 34s pour le calcul.


Il semble y avoir un comportement spécifique pour le `rfr` car il n'est pas dans le dump : son dossier y est, mais il est vide.

Cela explique peut-être pourquoi il est recalculé à chaque fois pour les quantiles.

Dans OpenFisca-France, il y a une `cache_blacklist` mais le `rfr` n'y est pas.

## Debug avec VS Code

Il faut créer un fichier .vscode/launch.json avec le contenu suivant :

```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python Debugger: Current File",
            "type": "debugpy",
            "request": "launch",
            "program": "${file}",
            "console": "integratedTerminal",
            "justMyCode": false,
        }
    ]
}
```

L'option importante est `"justMyCode": false,`, cela permet de voir le code de OpenFisca dans le debugger.

### Liste des calculs de RFR

J'ai mis une trace dans `simulation.calculate` de OpenFisca-core pour les calculs de RFR avec leur durée:


```sh
Calculate rfr for 2024 took 6115411548 nanosecondes
Calculate rfr for 2024 took 1211494161 nanosecondes
Calculate rfr for 2024 took  798162482 nanosecondes
Calculate rfr for 2024 took  771014170 nanosecondes
Calculate rfr for 2024 took  770828896 nanosecondes
Calculate rfr for 2024 took  800381382 nanosecondes
Calculate rfr for 2024 took  789667774 nanosecondes
Calculate rfr for 2022 took    4071520 nanosecondes
Calculate rfr for 2024 took  852572595 nanosecondes
Calculate rfr for 2024 took  856741077 nanosecondes
Calculate rfr for 2024 took  835108015 nanosecondes
Calculate rfr for 2024 took  841131705 nanosecondes
Calculate rfr for 2024 took  860157149 nanosecondes
Calculate rfr for 2022 took    8519099 nanosecondes
Calculate rfr for 2022 took    1440134 nanosecondes
Calculate rfr for 2024 took  918907679 nanosecondes
Calculate rfr for 2024 took  799577265 nanosecondes
Calculate rfr for 2024 took  917551836 nanosecondes
Calculate rfr for 2024 took  797834079 nanosecondes
Calculate rfr for 2024 took  802391545 nanosecondes
Calculate rfr for 2024 took  831138870 nanosecondes
Calculate rfr for 2024 took  841112803 nanosecondes
Calculate rfr for 2024 took  792228816 nanosecondes
Calculate rfr for 2024 took  784395125 nanosecondes
Calculate rfr for 2024 took  808854749 nanosecondes
Calculate rfr for 2024 took  797895804 nanosecondes
Calculate rfr for 2024 took  764301716 nanosecondes
Calculate rfr for 2024 took  772212292 nanosecondes
```

## Des pistes pour améliorer les temps de calcul

A la toute fin du calcul par OpenFisca-Core, le RFR est bien sauvegardé en mémoire, via le holder de `in_memory_storage.py`.


---
C'est holder.put_in_cache(array, period) simulation.py de Core qui sauve le RFR.
Ensuite il va bien dans self._set(period, value) de holder.py
Ensuite il calcul should_store_on_disk qui resort en False
Puis self._memory_storage.put(value, period)
Puis enfin put de in_memory_storage.py
Qui sauve bien `array([7380.8975, 1555.3782, 2375.1619], dtype=float32)`
A ce moment là, le RFR est bien sauvegardé en mémoire.

```py
self._memory_storage.get(period)
array([7380.8975, 1555.3782, 2375.1619], dtype=float32)

self.populations['foyer_fiscal']._holders['rfr']._memory_storage._arrays
{Period((<DateUnit.YEAR: 'year'>, Instant((2023, 1, 1)), 1)): array([7380.8975, 1555.3782, 2375.1619], dtype=float32)}
```

Pourtant, en sortie de calculate, le _memory_storage est vide :

```py
simulation.populations['foyer_fiscal']._holders['rfr']._memory_storage._arrays
{}
```

C'est parceque rfr est dans `invalidated_caches` et donc il est effacé en toute fin du calculate.

Cela vient d'OpenFisca Core. C'est `_calculate` qui appel `_check_for_cycle`, qui appel `invalidate_spiral_variables` qui appel `invalidate_cache_entry`.

La raison de l'inscription du RFR dans `invalidated_caches` provient du fait qu'il n'existe pas pour N-2. En lui injectant une valeur pour N-2, le RFR de l'année N est bien conservé en mémoire.

## Temps de calculs sur Ysabell le 24/09/2024

### Test amélioration RFR en input_variable en N-1 et N-2
IR avec réforme : 21 secondes
Sep 24 15:05:27 api-simu-etat-integ poetry[319583]: [leximpact-socio-fiscal-simu-etat INFO @ 15:05:27] API - call_compute_all_simulation_async - Calcul task_id='bd13c7bc-bff5-4cf7-ba70-7542be0db2ee' recu par redis en 21.72 secondes
--> Après livraison de la MR, le temps de calcul est passé à 17 secondes

AF avec réforme : 46 secondes
Sep 24 15:06:47 api-simu-etat-integ poetry[319938]: [leximpact-socio-fiscal-simu-etat INFO @ 15:06:47] API - call_compute_all_simulation_async - Calcul task_id='3621fd9e-5d8f-4c65-860a-dcdccbd304e2' recu par redis en 46.10 secondes
--> Après livraison de la MR, le temps de calcul est passé à 43 secondes

Cotisation MMID brut sans réforme : 50 secondes
Sep 24 15:10:02 api-simu-etat-integ poetry[319958]: [leximpact-socio-fiscal-simu-etat INFO @ 15:10:02] API - call_compute_all_simulation_async - Calcul task_id='4ffb8bba-943d-4369-af09-c2d73a5cda29' recu par redis en 50.54 secondes

Cotisation MMID brut avec réforme : 50 secondes
Sep 24 15:11:27 api-simu-etat-integ poetry[320142]: [leximpact-socio-fiscal-simu-etat INFO @ 15:11:27] API - call_compute_all_simulation_async - Calcul task_id='a8944069-0d0b-4715-a80f-4a1045e4977a' recu par redis en 50.08 secondes
--> Après livraison de la MR, le temps de calcul est passé à 18 secondes !!!

## Temps de calculs sur Ysabell le 30/09/2024

### Sans Dump avec ajout du RFR en N-1 et en N-2 dans l'injection

IR avec réforme : 18, 26, 16, 16

Cotis. alloc. familiales brute avec réforme : 22s, 19s, 16s

Cotisation MMID brut avec réforme : 18s, 19s, 18s

### Avec Dump avec ajout du RFR en N-1 et en N-2 dans l'injection

IR avec réforme : 11s, 18s (apres redémarrage worker), 11s

Cotis. alloc. familiales brute avec réforme : 21s (premier démarrage), 11s, 11s

Cotisation MMID brut avec réforme : 13s, 11s, 12s

=> Réduction des temps de calculs de 30% grace au dump.

## Vaut-il mieux 1 ou deux workers sur la même machine ?

Test sur Ysabelle avec la version PLF.

### 1 seul worker et 2 demandes de calculs IR

La première met 18 secondes, la deuxième 24, soit 42 secondes en tout.

### 2 workers et 2 demandes de calculs IR

La première met 31 secondes, la deuxième 32, soit 32 secondes en tout.
Problème : au deuxième essais les 16Go de mémoire sont saturés et on se retrouve bloqué.

2 workers en même temps sur le même serveur permettent de servir plus vite les usagers mais avec 16Go de RAM ça plante dès la deuxième simulation PLF en parallèle !

Cela montre qu'il faut louer des serveurs pour la période du PLF :
- On ne met aucun worker sur la prod Gaspode pour laisser le serveur aux calculs des cas-types.
- On loue 2 serveurs dédiés au budget. C'est le minimum car quand on touche au barème on demande vite 5 calculs budget en même temps !
