"""
Permet de lancer un worker pour traiter les simulations en attente dans la file Redis.
Cela permet de mieux gérer la charge et l'empreinte mémoire.
L'API reçoit les requêtes et les met dans une file Redis. Les workers récupèrent les requêtes et les traitent.
Pour tester en local:
- Monter les données : `sshfs ysabell:/data/private-data/output /mnt/data-out`
- Lancer redis-server avec docker : `docker run -d -p 6379:6379 --name redis redis:alpine`
- Lancer un worker : `poetry run python redis_worker.py --id 1`
- Lancer le test : `poetry run python tests/worker_test.py`
"""

import argparse
import asyncio
import json
import os

# import shutil
import unittest

# from pathlib import Path
from time import sleep, time

from leximpact_common_python_libraries.cache import Cache
from leximpact_common_python_libraries.config import Configuration
from leximpact_common_python_libraries.logger import Logger
from retrying import retry

from leximpact_socio_fisca_simu_etat.schema import (
    AllSimulationResult,
    NpEncoder,
    ReformeSocioFiscale,
)
from leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario import (
    compute_all_simulation,
    delete_survey_scenario,
)

tc = unittest.TestCase()
logger = Logger(package_name="leximpact-socio-fiscal-simu-etat").logger
worker_pid = os.getpid()
redis_bus = Cache(project_folder="leximpact-socio-fiscal-simu-etat")
worker_id = 1
config = Configuration(project_folder="leximpact-socio-fiscal-simu-etat")
REDIS_QUEUE_NAME = config.get("REDIS_QUEUE_NAME", fail_on_missing=False, default="dev")
restart_every_n_simulations = int(
    config.get("RESTART_EVERY_N_SIMULATIONS", fail_on_missing=False, default=20)
)
dump_path = None
if config.get("USE_DUMP", fail_on_missing=False, default="NO").lower().strip() == "yes":
    dump_path = config.get("DUMP_PATH", fail_on_missing=True)
    if not os.path.exists(dump_path):
        print(f"Le dossier {dump_path} n'existe pas !")
        exit(1)


def simulation(reform):
    resultat = asyncio.run(compute_all_simulation(reform, dump_path=dump_path))
    return resultat


@retry
def get_message():
    global redis_bus
    try:
        msg = redis_bus.cache.rpop(REDIS_QUEUE_NAME)
    except ConnectionError as e:
        logger.warning(
            f"redis_worker number {worker_id} (pid={worker_pid}) - get_message() : {e=}"
        )
        redis_bus = Cache(project_folder="leximpact-socio-fiscal-simu-etat")
        raise e
    return msg


@retry
def get_queue_lenght():
    global redis_bus
    try:
        lenght = redis_bus.cache.llen(REDIS_QUEUE_NAME)
    except ConnectionError as e:
        logger.warning(
            f"redis_worker number {worker_id} (pid={worker_pid}) - get_message() : {e=}"
        )
        redis_bus = Cache(project_folder="leximpact-socio-fiscal-simu-etat")
        raise e
    return lenght


@retry
def pull(msg: str):
    result = None
    error = None
    logger.info(
        f"redis_worker number {worker_id} (pid={worker_pid}) : Processing a message..."
    )

    try:
        msg = json.loads(msg)
    except Exception as e:
        error = f"Décodage JSON message {e}"

    jobid = msg.get("jobid")
    if jobid:
        try:
            reform = msg.get("reform")
            reform = json.loads(reform)
            reform = ReformeSocioFiscale.parse_obj(reform)
        except Exception as e:
            error = f"Décodage JSON de la réforme {e=}"
        if error is None:
            try:
                result = simulation(reform)
            except Exception as e:
                logger.error(f"Simulation {e}", exc_info=True)
                error = f"Simulation {e=}"
        if error is None:
            try:
                redis_bus.set(jobid, json.dumps(result, cls=NpEncoder))
                redis_bus.cache.expire(jobid, 60)
            except Exception as e:
                error = f"Envoi du résultat {e=}"
        if error:
            logger.error(
                f"redis_worker number {worker_id} (pid={worker_pid}) : ❌ Error while computing {msg=} {error=}"
            )
            result = AllSimulationResult(
                result={},
                errors=["Une erreur est survenue lors du calcul de la simulation."],
            )
            redis_bus.set(jobid, json.dumps(result, cls=NpEncoder))
            redis_bus.cache.expire(jobid, 60)
        else:
            logger.info(
                f"redis_worker number {worker_id} (pid={worker_pid}) : Processing done without error for {jobid}."
            )
    else:
        logger.warning(
            f"redis_worker number {worker_id} (pid={worker_pid}) : Message without 'jobid' {msg=}"
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--id", required=False)
    args = parser.parse_args()
    if args.id:
        worker_id = int(args.id)
    logger.info(
        f"redis_worker number {worker_id} (pid={worker_pid}) : Waiting for message..."
    )
    last_simulation = time()
    en_veille = True
    nb_simulation = 0
    while True:
        waiting_simu = get_queue_lenght()
        if en_veille and worker_id > 2 and waiting_simu < 4:
            # Nous sommes un worker bonus alors on ne prend pas les simulations si les autres peuvent les traiter plus vite que nous.
            # logger.info(
            #     f"redis_worker number {worker_id} (pid={worker_pid}) : Keep sleeping because {waiting_simu=} and {en_veille=}."
            # )
            sleep(10)
            continue
        msg = get_message()
        if msg is None:
            sleep(0.2)
            if (worker_id > 2) and (time() - last_simulation > 30 * 60):
                # Si pas de simulation depuis 30 minutes, on libère la mémoire
                logger.info(
                    f"redis_worker number {worker_id} (pid={worker_pid}) : 🛏️ Plus de simulation depuis 30 minutes, on passe en veille."
                )
                delete_survey_scenario()
                en_veille = True
        else:
            last_simulation = time()
            en_veille = False
            # Compute simulation
            pull(msg)
            nb_simulation += 1
            if nb_simulation % restart_every_n_simulations == 0:
                logger.info(
                    f"redis_worker number {worker_id} (pid={worker_pid}) : 💀 {nb_simulation} simulations traitées. Je m'auto-détruit pour libérer la mémoire. Systemd me relancera."
                )
                delete_survey_scenario()
                # delete_survey_scenario don't clean memory, we do a real exit()
                exit(0)
            queue_lenght = get_queue_lenght()
            if queue_lenght > 3:
                logger.warning(
                    f"redis_worker number {worker_id} (pid={worker_pid}) : 🔥 {queue_lenght} simulations en attente."
                )
