"""
Call the API https://api-simu-etat-integ.leximpact.dev with request and test the response

"""

import multiprocessing

import requests
from leximpact_common_python_libraries.config import Configuration

config = Configuration(project_folder="leximpact-socio-fiscal-simu-etat")


def call_api(payload):
    # url = "https://api-simu-etat-integ.leximpact.dev/state_simulation"
    url = "http://0.0.0.0:8000/default_state_simulation"
    header = {"jwt-token": config.get("TOKEN_FOR_LOAD_TESTING")}
    response = requests.post(url, json=payload, headers=header, timeout=1)
    if response.status_code != 200:
        print(f"Response code not 200 : {response.status_code}")
        return None
    return response.json()


# IRPP
payload = {
    "amendement": {
        "impot_revenu.bareme_ir_depuis_1945.bareme": {
            "scale": [
                {"rate": {"value": 0.01}, "threshold": {"value": 1}},
                {"rate": {"value": 0.11}, "threshold": {"value": 10994}},
                {"rate": {"value": 0.3}, "threshold": {"value": 28029}},
                {"rate": {"value": 0.41}, "threshold": {"value": 80142}},
                {"rate": {"value": 0.45}, "threshold": {"value": 172375}},
            ],
            "start": "2023-01-01",
            "type": "scale",
            "variable": "irpp_economique",
        }
    },
    "base": 2023,
    "output_variables": ["rfr", "irpp_economique"],
    "quantile_base_variable": ["rfr"],
    "quantile_compare_variables": ["irpp_economique"],
    "quantile_nb": 10,
}

# CSG
payload = {
    "base": 2023,
    "plf": 2024,
    "amendement": {
        "prelevements_sociaux.contributions_sociales.csg.activite.deductible.taux": 0.0001,
    },
    "output_variables": ["rfr", "csg_deductible_salaire", "csg_imposable_salaire"],
    "quantile_base_variable": ["rfr"],
    "quantile_compare_variables": ["csg_deductible_salaire", "csg_imposable_salaire"],
    "quantile_nb": 10,
}


def one_api_call():
    print("Call API with payload :", payload.get("base"))
    response = call_api(payload)
    try:
        print(
            "Montant :",
            response["result"]["base"]["state_budget"]["csg_deductible_salaire"],
        )
    except Exception as e:
        print("Error :", e)
        print("Response :", response)


processes = [multiprocessing.Process(target=one_api_call) for _ in range(4)]
for p in processes:
    p.start()
i = 0
for p in processes:
    p.join()
    i += 1
    print("XXXXXXXXXX FIN de ", i)
