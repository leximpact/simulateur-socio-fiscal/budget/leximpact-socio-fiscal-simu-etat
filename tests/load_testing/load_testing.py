from json import JSONDecodeError

from leximpact_common_python_libraries.config import Configuration
from locust import HttpUser, between, task

config = Configuration(project_folder="leximpact-socio-fiscal-simu-etat")

# Without Quantile
# payload = {
#     "base": 2021,
#     "plf": 2022,
#     "amendement": {
#         "prelevements_sociaux.contributions_sociales.csg.activite.deductible.taux": 0.0001,
#     },
#     "output_variables": [
#         "csg_imposable_salaire",
#     ],
#     "quantile_nb": 0,
# }

# With Quantile
payload = {
    "base": 2023,
    "plf": 2024,
    "amendement": {
        "prelevements_sociaux.contributions_sociales.csg.activite.deductible.taux": 0.0001,
    },
    "output_variables": ["rfr", "csg_deductible_salaire", "csg_imposable_salaire"],
    "quantile_base_variable": ["rfr"],
    "quantile_compare_variables": ["csg_deductible_salaire", "csg_imposable_salaire"],
    "quantile_nb": 10,
}


class ApiBudgetTest(HttpUser):
    host = "https://api-simu-etat-integ.leximpact.dev"
    # host = "http://0.0.0.0:8000"

    # Set the time to wait between two requests for one user
    wait_time = between(1, 5)

    def on_start(self):
        """
        On start we clear the API cache
        """
        self.clear_cache()

    @task
    def index(self):
        self.client.get("/")

    @task
    def status(self):
        self.client.get("/status")

    def clear_cache(self):
        """
        Clear the API cache
        """
        passwd = config.get("ADMIN_PASSWORD")
        with self.client.get(f"/clear_cache?secret={passwd}") as response:
            if response.status_code != 200:
                # response.failure(f"Response code not 200 : {response.status_code}")
                print(f"Response code not 200 : {response.status_code}")

    @task(5)
    def simu_csg(self):
        """
        Ask API for a simulation
        """
        header = {"jwt-token": config.get("TOKEN_FOR_LOAD_TESTING")}
        payload["amendement"][
            "prelevements_sociaux.contributions_sociales.csg.activite.deductible.taux"
        ] += 0.0001
        with self.client.post(
            # "/default_state_simulation",
            "/state_simulation",
            json=payload,
            catch_response=True,
            headers=header,
        ) as response:
            if response.status_code != 200:
                response.failure(f"Response code not 200 : {response.status_code}")
            try:
                csg = float(
                    response.json()["result"]["base"]["state_budget"][
                        "csg_imposable_salaire"
                    ]
                )
                if not (-100 * 1e9 < csg < -10 * 1e9):
                    response.failure(
                        f"Did not get expected value for 'csg_imposable_salaire': {response.json()['result']['base']['state_budget']['csg_imposable_salaire']}"
                    )
            except JSONDecodeError:
                response.failure(
                    f"Response could not be decoded as JSON: {response.text}"
                )
            except KeyError:
                response.failure(
                    f"Response did not contain expected key: {response.json()}"
                )
            except TypeError:
                response.failure(f"TypeError : {response.json()}")
