import asyncio
import cProfile
import unittest
from time import time

from leximpact_socio_fisca_simu_etat.schema import ReformeSocioFiscale
from leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario import (
    compute_all_simulation,
)

tc = unittest.TestCase()


def test_csg_retraite_base():
    reform = ReformeSocioFiscale(
        base=2024,
        quantile_nb=10,
        output_variables=[
            "rfr_par_part",
            "csg_deductible_retraite",
            "csg_imposable_retraite",
        ],
        quantile_base_variable=["rfr_par_part"],
        quantile_compare_variables=[
            "csg_deductible_retraite",
            "csg_imposable_retraite",
        ],
        plf=2024,
    )
    resultat = asyncio.run(compute_all_simulation(reform))
    csg_retraite = (
        resultat.result["base"].state_budget["csg_imposable_retraite"]
        + resultat.result["base"].state_budget["csg_deductible_retraite"]
    )
    tc.assertGreater(csg_retraite, -25 * 1e9)
    tc.assertLess(csg_retraite, -24 * 1e9)


if __name__ == "__main__":
    start = time()
    cProfile.run(
        "test_csg_retraite_base()",
        "profiling_simu-etat-dump-tmpfs-create_scenario.prof",
    )
    print(
        f"Temps d'exécution avec création du scénario: {time() - start:.2f} secondes\n\n"
    )

    start = time()
    cProfile.run(
        "test_csg_retraite_base()", "profiling_simu-etat-dump-tmpfs-reuse_scenario.prof"
    )
    print(
        f"Temps d'exécution : avec réutilisation du scénario {time() - start:.2f} secondes"
    )
