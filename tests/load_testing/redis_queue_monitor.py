from time import sleep

from leximpact_common_python_libraries.cache import Cache
from leximpact_common_python_libraries.config import Configuration

config = Configuration(project_folder="leximpact-socio-fiscal-simu-etat")

cache = Cache(project_folder="leximpact-socio-fiscal-simu-etat")

while True:
    sleep(1)
    print("DB whole size", cache.cache.dbsize())
    print(
        "DB waiting simulations :",
        cache.cache.llen(
            config.get("REDIS_QUEUE_NAME", fail_on_missing=False, default="dev")
        ),
    )
