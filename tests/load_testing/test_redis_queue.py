import json
import uuid
from time import sleep, time

from leximpact_common_python_libraries.cache import Cache
from leximpact_common_python_libraries.config import Configuration

from leximpact_socio_fisca_simu_etat.schema import ReformeSocioFiscale

cache = Cache(project_folder="leximpact-socio-fiscal-simu-etat")
config = Configuration(project_folder="leximpact-socio-fiscal-simu-etat")

# from rq import Queue


reform = ReformeSocioFiscale(
    base=2024,
    quantile_nb=10,
    output_variables=[
        "rfr_par_part",
        "csg_deductible_retraite",
        "csg_imposable_retraite",
    ],
    quantile_base_variable=["rfr_par_part"],
    quantile_compare_variables=[
        "csg_deductible_retraite",
        "csg_imposable_retraite",
    ],
    plf=2024,
)

jobid = str(uuid.uuid4())
msg = {"jobid": jobid, "reform": reform.json()}


def push(*values):
    cache.cache.rpush(
        config.get("REDIS_QUEUE_NAME", fail_on_missing=False, default="dev"), *values
    )


push(json.dumps(msg))

print(cache.get("clef"))

finished = False
start = time()
while not finished:
    sleep(1)
    result = cache.get(jobid)
    if result is not None:
        print(result)
        finished = True
    else:
        print("waiting for job to finish")
    print("time elapsed", time() - start)
    if time() - start > 10:
        print("timeout")
        finished = True


# q = Queue(connection=Redis())

# # from my_module import count_words_at_url
# # job = q.enqueue(count_words_at_url, 'http://nvie.com')
# # job = q.enqueue(test_csg_retraite_base)
# job = q.enqueue(singleton.do_something)
# # print(job.key)

# while not job.is_finished or job.is_failed:
#     print("waiting for job to finish")
#     if job.is_finished:
#         print('status : finished')
#     else:
#         print('status : not finished')
#     if job.is_queued:
#         print('status : in-queue')
#     if job.is_started:
#         print('status : waiting')
#     sleep(1)
# if job.is_failed:
#     print('status: failed')
# else:
#     print(job.latest_result().return_value)
