import unittest
from unittest.mock import patch

from leximpact_aggregates.aggregate import AggregateManager
from leximpact_common_python_libraries.config import Configuration

from leximpact_socio_fisca_simu_etat.schema import ReformeSocioFiscale

# from leximpact_socio_fisca_simu_etat.simu_budget import (
#     ReformeSocioFiscale,
#     check_aggregates,
#     compute_all_simulation,
# )
from leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario import (
    compute_all_simulation,
)

config = Configuration(env_filepath=".env")
agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))

period = 2025


class TestAF(unittest.IsolatedAsyncioTestCase):
    @patch(
        "leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario.config.get",
        return_value=False,
    )
    async def test_af(self, mock_config):

        def side_effect(key, default=None, **kwargs):
            if key == "USE_DUMP":
                return "NO"
            return config.get(param_name=key, default=default, **kwargs)

        mock_config.side_effect = side_effect
        reform = ReformeSocioFiscale(
            base=period,
            quantile_nb=10,
            output_variables=[
                "revenus_menage_par_uc",
                "af",
            ],
            quantile_base_variable=["revenus_menage_par_uc"],
            quantile_compare_variables=[
                "af",
                # "menage_avec_enfants",
            ],
            # plf=2024,
            winners_loosers_variable="af",
        )
        resultat = await compute_all_simulation(reform)
        self.assertIsNotNone(resultat.result["base"].state_budget)
        self.assertEqual(resultat.errors, [])
        self.assertIsNotNone(resultat.result["base"].state_budget.get("af"))
        af = resultat.result["base"].state_budget["af"]

        agg.load_aggregate(
            "CIBLES_PLF/PLF", "af", year=str(period), data_structure="unidimensional"
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = data.values["sum"]
        self.assertGreater(af, cible * 0.99)
        self.assertLess(af, cible * 1.01)
