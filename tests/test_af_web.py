"""
Ce test vise à reproduire le comportement du front pour mesurer les performances.
"""

import asyncio
import json
import time
import unittest
from unittest.mock import patch

from leximpact_common_python_libraries.config import Configuration

from leximpact_socio_fisca_simu_etat.schema import ReformeSocioFiscale
from leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario import (
    compute_all_simulation,
)

config = Configuration(env_filepath=".env")


class TestAllegementCotisationAllocationsFamiliales(unittest.IsolatedAsyncioTestCase):

    @patch(
        "leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario.config.get",
        return_value=False,
    )
    @unittest.skip("Désactivé, en attente du PLF 2026")
    async def test_allegement_cotisation_allocations_familiales_web(self, mock_config):

        def side_effect(key, default=None, **kwargs):
            if key == "USE_DUMP":
                return "NO"
            return config.get(param_name=key, default=default, **kwargs)

        mock_config.side_effect = side_effect

        start = time.time()
        with open("tests/amendement_af_17-09-2024.json") as f:
            reform_allocation = ReformeSocioFiscale.parse_obj(json.load(f))
        resultat = await compute_all_simulation(reform_allocation)
        self.assertIsNotNone(resultat.result["base"].state_budget)
        self.assertEqual(resultat.errors, [])
        self.assertIsNotNone(
            resultat.result["base"].state_budget.get(
                "allegement_cotisation_allocations_familiales"
            )
        )
        af = resultat.result["base"].state_budget[
            "allegement_cotisation_allocations_familiales"
        ]

        self.assertGreater(af, 8 * 1e9)
        self.assertLess(af, 12 * 1e9)

        # Décile
        self.assertEqual(len(resultat.result["amendement"].quantiles), 10)
        decile_af = resultat.result["amendement"].quantiles[2][
            "allegement_cotisation_allocations_familiales_sum"
        ]
        self.assertGreater(decile_af, 500 * 1e6)
        # self.assertLess(decile_af, 700 * 1e6)
        stop = time.time()
        if stop - start > 30:
            print(f"Temps de calcul : {stop - start:0.5f}s")


if __name__ == "__main__":
    test = TestAllegementCotisationAllocationsFamiliales()
    asyncio.run(test.test_allegement_cotisation_allocations_familiales_web())
