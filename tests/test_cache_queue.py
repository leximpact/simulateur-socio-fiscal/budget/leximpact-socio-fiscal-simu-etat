import unittest

from leximpact_common_python_libraries.cache import Cache
from leximpact_common_python_libraries.config import Configuration

cache = Cache(project_folder="leximpact-socio-fiscal-simu-etat")
config = Configuration(project_folder="leximpact-socio-fiscal-simu-etat")
REDIS_QUEUE_NAME = config.get("REDIS_QUEUE_NAME", fail_on_missing=False, default="dev")


class TestCacheQueue(unittest.TestCase):
    def test_queue(self):
        cache.cache.rpush(REDIS_QUEUE_NAME, "test")
        cache.cache.expire(REDIS_QUEUE_NAME, 60)
        msg = cache.cache.rpop(REDIS_QUEUE_NAME)
        self.assertEqual(msg, b"test")
        msg = cache.cache.rpop(REDIS_QUEUE_NAME)
        self.assertEqual(msg, None)
