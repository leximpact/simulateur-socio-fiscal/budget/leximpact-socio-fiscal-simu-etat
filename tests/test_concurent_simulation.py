"""
Run multiple simulation using survey scenario to validate concurent access to the same data works.

Run it manually with:
poetry run pytest -s tests/test_concurent_simulation.py

"""

# import multiprocessing  # Not needed with asyncio
import pytest

from leximpact_socio_fisca_simu_etat.schema import ReformeSocioFiscale
from leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario import (
    compute_all_simulation,
)


async def reforme_irpp_plf():
    reform = ReformeSocioFiscale(
        base=2023,
        plf=2024,
        amendement={
            "impot_revenu.bareme_ir_depuis_1945.bareme": {
                "scale": [
                    {"rate": {"value": 0.0}, "threshold": {"value": 1}},
                    {"rate": {"value": 0.11}, "threshold": {"value": 10_000}},
                    {"rate": {"value": 0.30}, "threshold": {"value": 27_000}},
                    {"rate": {"value": 0.41}, "threshold": {"value": 80_000}},
                    {"rate": {"value": 0.45}, "threshold": {"value": 172_000}},
                ],
                "start": "2023-01-01",
                "type": "scale",
                "variable": "irpp_economique",
            }
        },
        output_variables=["rfr", "irpp_economique"],
        quantile_nb=10,
        quantile_base_variable=["rfr"],
        quantile_compare_variables=[
            "irpp_economique",
        ],
        winners_loosers_variable="irpp_economique",
    )
    resultat = await compute_all_simulation(reform)
    irpp = resultat.result["amendement"].state_budget["irpp_economique"]
    print(irpp)
    # assertGreater(irpp, -90 * 1e9)
    # assertLess(irpp, -80 * 1e9)


@pytest.mark.skip(reason="Not needed all time")
def test_concurent_simulation():
    for i in range(2):
        reforme_irpp_plf()
    # As of first September 2023, both fork and spawn work well
    # multiprocessing.set_start_method("spawn")
    # # multiprocessing.set_start_method("fork")
    # processes = [multiprocessing.Process(target=reforme_irpp_plf) for _ in range(2)]
    # for p in processes:
    #     p.start()
    # i = 0
    # for p in processes:
    #     p.join()
    #     i += 1
    #     print("XXXXXXXXXX FIN de ", i)
