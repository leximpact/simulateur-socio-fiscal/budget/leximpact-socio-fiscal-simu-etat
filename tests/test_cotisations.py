import unittest
from unittest.mock import patch

from leximpact_aggregates.aggregate import AggregateManager
from leximpact_common_python_libraries.config import Configuration

from leximpact_socio_fisca_simu_etat.schema import ReformeSocioFiscale

# from leximpact_socio_fisca_simu_etat.simu_budget import (
#     ReformeSocioFiscale,
#     check_aggregates,
#     compute_all_simulation,
# )
from leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario import (
    compute_all_simulation,
)

config = Configuration(env_filepath=".env")
period = 2025


class TestCotisations(unittest.IsolatedAsyncioTestCase):
    @patch(
        "leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario.config.get",
        return_value=False,
    )
    async def test_cotisations_base(self, mock_config):

        def side_effect(key, default=None, **kwargs):
            if key == "USE_DUMP":
                return "NO"
            return config.get(param_name=key, default=default, **kwargs)

        mock_config.side_effect = side_effect
        reform = ReformeSocioFiscale(
            base=period,
            quantile_nb=10,
            output_variables=[
                "rfr_par_part",
                "agirc_arrco_employeur",
                "agirc_arrco_salarie",
                "contribution_equilibre_general_employeur",
                "contribution_equilibre_general_salarie",
                "vieillesse_deplafonnee_salarie",
                "vieillesse_plafonnee_salarie",
                "vieillesse_deplafonnee_employeur",
                "vieillesse_plafonnee_employeur",
                "allegement_general",
                "mmid_employeur_net_allegement",
                "allegement_cotisation_maladie",
                "famille_net_allegement",
                "allegement_cotisation_allocations_familiales",
                "contribution_solidarite_autonomie",
                "chomage_employeur",
                "ags",
                "fnal_contribution",
            ],
            quantile_base_variable=["rfr_par_part"],
            quantile_compare_variables=[
                "csg_deductible_retraite",
                "csg_imposable_retraite",
            ],
            winners_loosers_variable="csg_retraite",
        )
        resultat = await compute_all_simulation(reform)
        self.assertIsNotNone(resultat.result["base"].state_budget)
        self.assertEqual(resultat.errors, [])

        agirc_arrco = (
            resultat.result["base"].state_budget["agirc_arrco_employeur"]
            + resultat.result["base"].state_budget["agirc_arrco_salarie"]
            + resultat.result["base"].state_budget[
                "contribution_equilibre_general_employeur"
            ]
            + resultat.result["base"].state_budget[
                "contribution_equilibre_general_salarie"
            ]
        )
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/PLF",
            "agirc_arrco",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = data.values["sum"]
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/PLF",
            "allegement_general_part_agirc_arrco",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = cible + data.values["sum"]
        self.assertGreater(agirc_arrco, -cible * 1.01, "Cotisations agirc-arrco")
        self.assertLess(agirc_arrco, -cible * 0.99, "Cotisations agirc-arrco")

        vieillesse = (
            resultat.result["base"].state_budget["vieillesse_deplafonnee_salarie"]
            + resultat.result["base"].state_budget["vieillesse_plafonnee_salarie"]
            + resultat.result["base"].state_budget["vieillesse_deplafonnee_employeur"]
            + resultat.result["base"].state_budget["vieillesse_plafonnee_employeur"]
        )
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/PLF",
            "cotisations_retraite_cnav",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = data.values["sum"]
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/PLF",
            "allegement_general_part_vieillesse",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = cible + data.values["sum"]
        self.assertGreater(vieillesse, -cible * 1.01, "Cotisations vieillesse")
        self.assertLess(vieillesse, -cible * 0.99, "Cotisations vieillesse")

        allegement_general = resultat.result["base"].state_budget["allegement_general"]
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/CONTREFACTUEL",  # on met en contrefactuel car en 2025 ce qui était prévu dans le PLF initial n'est pas passé pour 2025
            "allegement_general",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = data.values["sum"]
        # self.assertGreater(allegement_general, cible*0.99, "Allègement général")
        self.assertLess(allegement_general, cible * 1.01, "Allègement général")

        allegement_cotisation_maladie = resultat.result["base"].state_budget[
            "allegement_cotisation_maladie"
        ]
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/CONTREFACTUEL",  # on met en contrefactuel car en 2025 ce qui était prévu dans le PLF initial n'est pas passé pour 2025
            "allegement_cotisation_maladie",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = data.values["sum"]
        # self.assertGreater(allegement_cotisation_maladie, cible*0.99, "Allègement cotisation maladie")
        self.assertLess(
            allegement_cotisation_maladie, cible * 1.01, "Allègement cotisation maladie"
        )

        mmid_net_allegement = resultat.result["base"].state_budget[
            "mmid_employeur_net_allegement"
        ]
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/CONTREFACTUEL",  # on met en contrefactuel car en 2025 ce qui était prévu dans le PLF initial n'est pas passé pour 2025
            "mmid_employeur_net_allegement",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = data.values["sum"]
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/PLF",
            "allegement_general_part_mmid",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = cible + data.values["sum"]
        self.assertGreater(
            mmid_net_allegement, -cible * 1.01, "Cotisation maladie nette"
        )
        self.assertLess(mmid_net_allegement, -cible * 0.99, "Cotisation maladie nette")

        allegement_cotisation_allocations_familiales = resultat.result[
            "base"
        ].state_budget["allegement_cotisation_allocations_familiales"]
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/CONTREFACTUEL",  # on met en contrefactuel car en 2025 ce qui était prévu dans le PLF initial n'est pas passé pour 2025
            "allegement_cotisation_allocations_familiales",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = data.values["sum"]
        self.assertGreater(
            allegement_cotisation_allocations_familiales,
            cible * 0.99,
            "Allègement cotisation allocations familiales",
        )
        self.assertLess(
            allegement_cotisation_allocations_familiales,
            cible * 1.01,
            "Allègement cotisation allocations familiales",
        )

        famille_net_allegement = resultat.result["base"].state_budget[
            "famille_net_allegement"
        ]
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/CONTREFACTUEL",  # on met en contrefactuel car en 2025 ce qui était prévu dans le PLF initial n'est pas passé pour 2025
            "famille_net_allegement",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = data.values["sum"]
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/PLF",
            "allegement_general_part_famille",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = cible + data.values["sum"]
        self.assertGreater(
            famille_net_allegement,
            -cible * 1.01,
            "Cotisation allocations familles nette",
        )
        self.assertLess(
            famille_net_allegement,
            -cible * 0.99,
            "Cotisation allocations familles nette",
        )

        contribution_solidarite_autonomie = resultat.result["base"].state_budget[
            "contribution_solidarite_autonomie"
        ]
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/PLF",
            "contribution_solidarite_autonomie",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = data.values["sum"]
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/PLF",
            "allegement_general_part_csa",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = cible + data.values["sum"]
        self.assertGreater(
            contribution_solidarite_autonomie,
            -cible * 1.01,
            "Contribution solidarité autonomie nette",
        )
        self.assertLess(
            contribution_solidarite_autonomie,
            -cible * 0.99,
            "Contribution solidarité autonomie nette",
        )

        cotisation_chomage = (
            resultat.result["base"].state_budget["chomage_employeur"]
            + resultat.result["base"].state_budget["ags"]
        )
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/PLF",
            "cotisations_chomage",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = data.values["sum"]
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/PLF",
            "allegement_general_part_chomage",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = cible + data.values["sum"]
        self.assertGreater(
            cotisation_chomage, -cible * 1.01, "Cotisations chomage nettes"
        )
        self.assertLess(cotisation_chomage, -cible * 0.99, "Cotisations chomage nettes")

        fnal_contribution = resultat.result["base"].state_budget["fnal_contribution"]
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/PLF",
            "fnal_contribution",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = data.values["sum"]
        agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))
        agg.load_aggregate(
            "CIBLES_PLF/PLF",
            "allegement_general_part_fnal",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = cible + data.values["sum"]
        self.assertGreater(fnal_contribution, -cible * 1.01, "Cotisation fnal")
        self.assertLess(fnal_contribution, -cible * 0.99, "Cotisation fnal")
