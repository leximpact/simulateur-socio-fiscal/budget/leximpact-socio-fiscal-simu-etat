import unittest
from unittest.mock import patch

from leximpact_aggregates.aggregate import AggregateManager
from leximpact_common_python_libraries.config import Configuration

from leximpact_socio_fisca_simu_etat.schema import ReformeSocioFiscale

# from leximpact_socio_fisca_simu_etat.simu_budget import (
#     ReformeSocioFiscale,
#     check_aggregates,
#     compute_all_simulation,
# )
from leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario import (
    compute_all_simulation,
)

config = Configuration(env_filepath=".env")
agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))

period = 2025


class TestCSGRetraite(unittest.IsolatedAsyncioTestCase):
    @patch(
        "leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario.config.get",
        return_value=False,
    )
    async def test_csg_retraite_base(self, mock_config):

        def side_effect(key, default=None, **kwargs):
            if key == "USE_DUMP":
                return "NO"
            return config.get(param_name=key, default=default, **kwargs)

        mock_config.side_effect = side_effect
        reform = ReformeSocioFiscale(
            base=period,
            quantile_nb=10,
            output_variables=[
                "rfr_par_part",
                "csg_deductible_retraite",
                "csg_imposable_retraite",
            ],
            quantile_base_variable=["rfr_par_part"],
            quantile_compare_variables=[
                "csg_deductible_retraite",
                "csg_imposable_retraite",
            ],
            winners_loosers_variable="csg_retraite",
        )
        resultat = await compute_all_simulation(reform)
        self.assertIsNotNone(resultat.result["base"].state_budget)
        self.assertEqual(resultat.errors, [])
        self.assertIsNotNone(
            resultat.result["base"].state_budget.get("csg_imposable_retraite")
        )
        csg_retraite = (
            resultat.result["base"].state_budget["csg_imposable_retraite"]
            + resultat.result["base"].state_budget["csg_deductible_retraite"]
        )
        agg.load_aggregate(
            "CIBLES_PLF/PLF",
            "csg_retraite",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = data.values["sum"]
        self.assertGreater(csg_retraite, -cible * 1.01, "csg retraite")
        self.assertLess(csg_retraite, -cible * 0.99, "csg retraite")
