import unittest
from unittest.mock import patch

import pytest
from leximpact_aggregates.aggregate import AggregateManager
from leximpact_common_python_libraries.config import Configuration

from leximpact_socio_fisca_simu_etat.schema import ReformeSocioFiscale

# from leximpact_socio_fisca_simu_etat.simu_budget import (
#     ReformeSocioFiscale,
#     check_aggregates,
#     compute_all_simulation,
# )
from leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario import (
    compute_all_simulation,
)

tc = unittest.TestCase()

config = Configuration(env_filepath=".env")
agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))

period = 2025


class TestCSGSalaire(unittest.IsolatedAsyncioTestCase):
    # def test_check_aggregates(self):
    #     reform = ReformeSocioFiscale(
    #         base=2022,
    #         output_variables=["csg_imposable_salaire", "csg_deductible_salaire"],
    #         quantile_nb=0,
    #     )
    #     resultat = compute_all_simulation(reform)
    #     self.assertTrue(check_aggregates(reform, resultat))

    # def test_check_aggregates_error(self):
    #     reform = ReformeSocioFiscale(
    #         base=2022,
    #         output_variables=["csg_imposable_salaire", "csg_deductible_salaire"],
    #         quantile_nb=0,
    #     )
    #     resultat = compute_all_simulation(reform)
    #     resultat.result["base"].state_budget["csg_deductible_salaire"] = 0
    #     self.assertFalse(check_aggregates(reform, resultat))
    #     # self.assertTrue(check_aggregates(reform, resultat))
    @patch(
        "leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario.config.get",
        return_value=False,
    )
    async def test_csg_base(self, mock_config):

        def side_effect(key, default=None, **kwargs):
            if key == "USE_DUMP":
                return "NO"
            return config.get(param_name=key, default=default, **kwargs)

        mock_config.side_effect = side_effect
        reform = ReformeSocioFiscale(
            base=period,
            output_variables=["csg_salaire"],
            quantile_nb=0,
        )
        resultat = await compute_all_simulation(reform)
        csg_salaire = resultat.result["base"].state_budget["csg_salaire"]
        agg.load_aggregate(
            "CIBLES_PLF/PLF",
            "csg_salaire",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = data.values["sum"]
        self.assertGreater(csg_salaire, -cible * 1.01, "csg salaire")
        self.assertLess(csg_salaire, -cible * 0.99, "csg salaire")

    @pytest.mark.skip(reason="Ce test est valable uniquement en période de PLF")
    async def test_csg_plf(self):
        reform = ReformeSocioFiscale(
            base=2024,
            plf=2024,
            output_variables=["csg_salaire"],
            quantile_nb=0,
        )
        resultat = await compute_all_simulation(reform)
        csg_salaire = resultat.result["plf"].state_budget["csg_salaire"]
        self.assertGreater(csg_salaire, -92.3 * 1e9)
        self.assertLess(csg_salaire, -92.2 * 1e9)

    # async def test_csg_base_reforme(self):
    #     reform = ReformeSocioFiscale(
    #         base=2024,
    #         amendement={
    #             "prelevements_sociaux.contributions_sociales.csg.activite.deductible.taux": {
    #                 "start": "2023-01-01",
    #                 "type": "scale",
    #                 "variable": "csg_salaire",
    #                 "scale": [{"rate": {"value": 0.023}, "threshold": {"value": 0}}],
    #             }
    #         },
    #         output_variables=[
    #             "rfr_par_part",
    #             "csg_salaire",
    #         ],
    #         quantile_nb=0,
    #     )
    #     resultat = await compute_all_simulation(reform)
    #     csg_salaire = resultat.result["amendement"].state_budget["csg_salaire"]
    #     self.assertGreater(csg_salaire, -47.2 * 1e9)
    #     self.assertLess(csg_salaire, -47.1 * 1e9)

    """
Requête du front :
{
  "amendement": {
    "prelevements_sociaux.contributions_sociales.csg.activite.deductible.taux": {
      "start": "2023-01-01",
      "type": "parameter",
      "variable": "csg_salaire",
      "value": 0.023
    }
  },
  "base": 2024,
  "output_variables": [
    "rfr_par_part",
    "csg_deductible_salaire",
    "csg_imposable_salaire"
  ],
  "quantile_base_variable": [
    "rfr_par_part"
  ],
  "quantile_compare_variables": [
    "csg_deductible_salaire",
    "csg_imposable_salaire"
  ],
  "quantile_nb": 10
}
    """

    @pytest.mark.skip(reason="Ce test est valable uniquement en période de PLF")
    async def test_csg_plf_reforme(self):
        reform = ReformeSocioFiscale(
            base=2024,
            plf=2024,
            amendement={
                "prelevements_sociaux.contributions_sociales.csg.activite.imposable.taux": {
                    "start": "2023-01-01",
                    "type": "parameter",
                    "value": 0.027,
                }
            },
            output_variables=[
                "rfr_par_part",
                "csg_deductible_salaire",
                "csg_imposable_salaire",
            ],
            quantile_nb=10,
            quantile_base_variable=["rfr_par_part"],
            quantile_compare_variables=[
                "csg_deductible_salaire",
                "csg_imposable_salaire",
            ],
            winners_loosers_variable="csg_retraite",
        )
        resultat = await compute_all_simulation(reform)
        csg_salaire = (
            resultat.result["amendement"].state_budget["csg_imposable_salaire"]
            + resultat.result["amendement"].state_budget["csg_deductible_salaire"]
        )
        self.assertGreater(csg_salaire, -96 * 1e9)
        self.assertLess(csg_salaire, -95 * 1e9)
        print(resultat.result["amendement"].compare_before_after)
        tc.assertEqual(
            resultat.result["amendement"]
            .compare_before_after["amendement_plf"]
            .non_zero_after,
            resultat.result["plf"].compare_before_after["plf"].non_zero_after,
        )
        tc.assertEqual(
            resultat.result["amendement"]
            .compare_before_after["amendement_plf"]
            .above_after,
            0,
        )
        tc.assertEqual(
            resultat.result["plf"].compare_before_after["plf"].above_after, 0
        )
        tc.assertGreater(
            resultat.result["amendement"]
            .compare_before_after["amendement_plf"]
            .lower_after,
            resultat.result["plf"].compare_before_after["plf"].lower_after,
        )
