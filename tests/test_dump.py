"""

"""

import os
import shutil
import time

import numpy as np
from openfisca_core.periods import DateUnit, Instant, Period
from openfisca_core.simulation_builder import SimulationBuilder
from openfisca_core.tools.simulation_dumper import dump_simulation
from openfisca_france import FranceTaxBenefitSystem

dump_path = "/tmp/dump_openfisca_lqzkgsdj"

tbs = FranceTaxBenefitSystem()

sb = SimulationBuilder()
simulation = sb.build_default_simulation(tbs, count=3)
instant = Instant((2023, 1, 1))

period = Period((DateUnit.YEAR, instant, 1))
simulation.set_input("salaire_de_base", period, np.array([10_000, 2500, 3500]))
simulation.set_input(
    "salaire_de_base",
    Period((DateUnit.YEAR, Instant((2022, 1, 1)), 1)),
    np.array([10_000, 2500, 3500]),
)
simulation.set_input(
    "salaire_de_base",
    Period((DateUnit.YEAR, Instant((2021, 1, 1)), 1)),
    np.array([10_000, 2500, 3500]),
)
# simulation.set_input("rfr", Period((DateUnit.YEAR, Instant((2021, 1, 1)), 1)), np.array([1234, 1234, 1234]))
simulation.set_input(
    "rfr", Period((DateUnit.YEAR, Instant((2021, 1, 1)), 1)), np.array([0, 0, 0])
)


# white_list = [  {"variable": "rfr", "period": period}]
white_list = None

# activate the trace
# simulation.trace = True
# print(f"{simulation.max_spiral_loops=}")
# # simulation.max_spiral_loops = 3
# print(f"{simulation.max_spiral_loops=}")
for i in range(1):
    start = time.time()
    rfr = simulation.calculate("rfr", period)  # , white_list=white_list
    print(
        f"Temps de calcul RFR {i+1} fois avec {white_list=} : {time.time() - start:0.5f}s"
    )

print("############ Invalidated caches :")
for invalidated_cache in simulation.invalidated_caches:
    print(f"{invalidated_cache.variable} - {invalidated_cache.period}")
print("############ Invalidated caches -end")

# print calculation steps
# simulation.tracer.print_computation_log()


# rfr = simulation.calculate("irpp_economique", period)
if rfr[0] > 100:
    print(f"Le RFR a bien été calculé : {rfr}")
else:
    print("ERROR", rfr)
    exit(1)
if os.path.exists(dump_path):
    # Remove folder
    shutil.rmtree(dump_path)


dump_simulation(simulation, dump_path)

if os.path.exists(dump_path + "/rfr_plus_values_hors_rni/2023.npy"):
    print("Le rfr_plus_values_hors_rni est bien dans le dump.")
    if os.path.exists(dump_path + "/rfr/2023.npy"):
        print("OK, le rfr est aussi dans le dump.")
    else:
        print("ERROR : Le RFR n'est pas dans le dump alors qu'il vient dêtre calculé.")
        exit(1)
