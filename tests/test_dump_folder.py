import os
import shutil
import unittest
from pathlib import Path

import pytest
from leximpact_survey_scenario.leximpact_survey_scenario import leximpact_tbs

from leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario import (
    create_survey_scenario,
)

use_dump_before = os.environ.get("USE_DUMP")

period = 2025


@pytest.fixture(autouse=True)
def run_around_tests():
    # Code that will run before your test, for example:
    os.environ["USE_DUMP"] = "YES"
    # A test function will be run at this point
    yield
    print("run_around_tests", os.environ.get("USE_DUMP"))
    # Code that will run after your test, for example:
    if use_dump_before:
        os.environ["USE_DUMP"] = use_dump_before


class TestDump(unittest.IsolatedAsyncioTestCase):
    async def test_folder_dont_exist(self):
        # Assert ValueError is raised when dump_path don't exist
        with self.assertRaises(ValueError):
            _ = await create_survey_scenario(
                period=period,
                baseline_tax_benefit_system=leximpact_tbs,
                amendement_tax_benefit_system=leximpact_tbs,
                plf_tax_benefit_system=leximpact_tbs,
                dump_path="/tmp/folder_not_existing/folder_not_existing",
                computed_variables_to_dump=[],
            )

    async def test_folder_none(self):
        # Assert ValueError is raised when dump_path don't exist
        with self.assertRaises(ValueError):
            _ = await create_survey_scenario(
                period=period,
                baseline_tax_benefit_system=leximpact_tbs,
                amendement_tax_benefit_system=leximpact_tbs,
                plf_tax_benefit_system=leximpact_tbs,
                dump_path=None,
                computed_variables_to_dump=[],
            )

    async def test_folder_exist(self):
        dump_path = Path("/tmp/dump_openfisca_test_folder_exist")
        if dump_path.exists():
            shutil.rmtree(dump_path)
        survey_scenario = await create_survey_scenario(
            period=period,
            baseline_tax_benefit_system=leximpact_tbs,
            amendement_tax_benefit_system=leximpact_tbs,
            plf_tax_benefit_system=None,
            dump_path=dump_path,
            computed_variables_to_dump=[],
        )
        self.assertTrue(survey_scenario)
        self.assertTrue(dump_path.joinpath("baseline/rfr/2023.npy").exists())
        # Clean up
        if dump_path.exists():
            shutil.rmtree(dump_path)

    async def test_folder_empty_exist(self):
        dump_path = Path("/tmp/dump_openfisca_test_folder_empty_exist")
        if dump_path.exists():
            shutil.rmtree(dump_path)
        dump_path.mkdir()
        with self.assertRaises(ValueError):
            _ = await create_survey_scenario(
                period=period,
                baseline_tax_benefit_system=leximpact_tbs,
                amendement_tax_benefit_system=leximpact_tbs,
                plf_tax_benefit_system=None,
                dump_path=dump_path,
                computed_variables_to_dump=[],
            )
        shutil.rmtree(dump_path)

    async def test_folder_plf_dont_exist(self):
        dump_path = Path("/tmp/dump_openfisca_test_folder_plf_dont_exist")
        if dump_path.exists():
            shutil.rmtree(dump_path)
        dump_path.mkdir()
        with self.assertRaises(ValueError):
            _ = await create_survey_scenario(
                period=period,
                baseline_tax_benefit_system=leximpact_tbs,
                amendement_tax_benefit_system=leximpact_tbs,
                plf_tax_benefit_system=leximpact_tbs,
                dump_path=dump_path,
                computed_variables_to_dump=[],
            )
        shutil.rmtree(dump_path)

    async def test_folder_plf_exist(self):
        dump_path = Path("/tmp/dump_openfisca_test_folder_plf_exist")
        if dump_path.exists():
            shutil.rmtree(dump_path)
        Path.joinpath(dump_path, "plf").mkdir(parents=True)
        Path.joinpath(dump_path, "baseline").mkdir(parents=True)
        with self.assertRaises(FileNotFoundError):
            _ = await create_survey_scenario(
                period=period,
                baseline_tax_benefit_system=leximpact_tbs,
                amendement_tax_benefit_system=leximpact_tbs,
                plf_tax_benefit_system=leximpact_tbs,
                dump_path=dump_path,
                computed_variables_to_dump=[],
            )
        shutil.rmtree(dump_path)
