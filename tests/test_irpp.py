import os
import unittest

import pytest
from leximpact_aggregates.aggregate import AggregateManager
from leximpact_common_python_libraries.config import Configuration

from leximpact_socio_fisca_simu_etat.schema import ReformeSocioFiscale
from leximpact_socio_fisca_simu_etat.simu_budget_survey_scenario import (
    compute_all_simulation,
)

use_dump_before = os.environ.get("USE_DUMP")

config = Configuration(env_filepath=".env")
agg = AggregateManager(aggregates_path=config.get("AGREGATS_PATH"))

period = 2025


@pytest.fixture(autouse=True)
def run_around_tests():
    # Code that will run before your test, for example:
    os.environ["USE_DUMP"] = "NO"
    # A test function will be run at this point
    yield
    print("run_around_tests", os.environ.get("USE_DUMP"))
    # Code that will run after your test, for example:
    if use_dump_before:
        os.environ["USE_DUMP"] = use_dump_before


class TestIRPP(unittest.IsolatedAsyncioTestCase):

    @pytest.mark.skip(reason="Ce test est valable uniquement en période sans PLF")
    async def test_irpp_base(self):
        reform = ReformeSocioFiscale(
            base=period,
            output_variables=["rfr", "irpp_economique"],
            quantile_nb=10,
            quantile_base_variable=["rfr"],
            quantile_compare_variables=["irpp_economique"],
            winners_loosers_variable="irpp_economique",
        )
        resultat = await compute_all_simulation(reform)
        irpp = resultat.result["base"].state_budget["irpp_economique"]
        agg.load_aggregate(
            "CIBLES_PLF/PLF",
            "irpp_economique",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = data.values["sum"]
        self.assertGreater(irpp, -cible * 1.01, "Impot sur le revenu")
        self.assertLess(irpp, -cible * 0.99, "Impot sur le revenu")

    @pytest.mark.skip(reason="Ce test est valable uniquement en période de PLF")
    async def test_irpp_plf(self):
        reform = ReformeSocioFiscale(
            base=period,
            plf=period,
            output_variables=["rfr", "irpp_economique"],
            quantile_nb=10,
            quantile_base_variable=["rfr"],
            quantile_compare_variables=["irpp_economique"],
            winners_loosers_variable="irpp_economique",
        )
        resultat = await compute_all_simulation(reform)
        irpp = resultat.result["plf"].state_budget["irpp_economique"]
        agg.load_aggregate(
            "CIBLES_PLF/PLF",
            "irpp_economique",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = data.values["sum"]
        self.assertGreater(irpp, -cible * 1.01, "Impot sur le revenu")
        self.assertLess(irpp, -cible * 0.99, "Impot sur le revenu")
        irpp = resultat.result["base"].state_budget["irpp_economique"]
        agg.load_aggregate(
            "CIBLES_PLF/CONTREFACTUEL",
            "irpp_economique",
            year=str(period),
            data_structure="unidimensional",
        )
        for data in agg.aggregate.data:
            if data.date == str(period):
                cible = data.values["sum"]
        self.assertGreater(irpp, -cible * 1.01, "Impot sur le revenu")
        self.assertLess(irpp, -cible * 0.99, "Impot sur le revenu")

    @pytest.mark.skip(reason="Ce test est valable uniquement en période de PLF")
    async def test_irpp_plf_amendement(self):
        reform = ReformeSocioFiscale(
            base=2024,
            plf=2024,
            amendement={
                "impot_revenu.bareme_ir_depuis_1945.bareme": {
                    "scale": [
                        {"rate": {"value": 0.0}, "threshold": {"value": 1}},
                        {"rate": {"value": 0.11}, "threshold": {"value": 10_000}},
                        {"rate": {"value": 0.30}, "threshold": {"value": 27_000}},
                        {"rate": {"value": 0.41}, "threshold": {"value": 80_000}},
                        {"rate": {"value": 0.45}, "threshold": {"value": 172_000}},
                    ],
                    "start": "2023-01-01",
                    "type": "scale",
                    "variable": "irpp_economique",
                }
            },
            output_variables=["rfr", "irpp_economique"],
            quantile_nb=10,
            quantile_base_variable=["rfr"],
            quantile_compare_variables=["irpp_economique"],
            winners_loosers_variable="irpp_economique",
        )
        resultat = await compute_all_simulation(reform)
        irpp = resultat.result["amendement"].state_budget["irpp_economique"]
        self.assertGreater(irpp, -105 * 1e9)
        self.assertLess(irpp, -101 * 1e9)
