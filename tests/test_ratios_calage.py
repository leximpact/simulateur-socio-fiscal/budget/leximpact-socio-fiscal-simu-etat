import pytest
from leximpact_survey_scenario.leximpact_survey_scenario import leximpact_tbs
from openfisca_france_reforms.contrefactuel_plf import ContrefactuelPlf

try:
    from openfisca_france_reforms.plf_plfss_2026 import PlfPlfss2026

    PLF = True
except ImportError:
    PLF = False
    print("WARNING : Impossible d'importer PlfPlfss2026")
from leximpact_socio_fisca_simu_etat.assets.ratios_calage_baseline import (
    ratios_calage_baseline,
)
from leximpact_socio_fisca_simu_etat.assets.ratios_calage_contrefactuel import (
    ratios_calage_contrefactuel,
)
from leximpact_socio_fisca_simu_etat.assets.ratios_calage_plf import ratios_calage_plf
from leximpact_socio_fisca_simu_etat.ratios_calage import (
    compute_ratios_plf_contrefactuel,
    ratios_contrefactuel,
    ratios_plf,
)


def test_ratios_baseline():
    ratios_test_baseline = compute_ratios_plf_contrefactuel(
        period=2025,
        tax_benefits_system=leximpact_tbs,
        ratios=ratios_plf,
        ratios_name="baseline",
        save_ratios=False,
    )

    assert (
        ratios_test_baseline == ratios_calage_baseline
    ), "les ratios de calage pour la baseline ont changé, peut être faut il les recalculer ?"


@pytest.mark.skip(reason="Ce test est valable uniquement en période de PLF")
def test_ratios_plf():
    if not PLF:
        pytest.skip(
            "Ce test est valable uniquement en période de PLF, or PlfPlfss2026 n'est pas importé"
        )
    leximpact_tbs_plf = PlfPlfss2026(leximpact_tbs)

    ratios_test_plf = compute_ratios_plf_contrefactuel(
        period=2025,
        tax_benefits_system=leximpact_tbs_plf,
        ratios=ratios_plf,
        ratios_name="test_plf",
        save_ratios=False,
    )

    assert (
        ratios_test_plf == ratios_calage_plf
    ), "les ratios de calage pour le plf ont changé, peut être faut il les recalculer ?"


@pytest.mark.skip(reason="Ce test est valable uniquement en période de PLF")
def test_ratios_contrefactuel():
    leximpact_tbs_contref = ContrefactuelPlf(leximpact_tbs)

    ratios_test_contref = compute_ratios_plf_contrefactuel(
        period=2025,
        tax_benefits_system=leximpact_tbs_contref,
        ratios=ratios_contrefactuel,
        ratios_name="test_contrefactuel",
        save_ratios=False,
    )

    assert (
        ratios_test_contref == ratios_calage_contrefactuel
    ), "les ratios de calage pour le contrefactuel ont changé, peut être faut il les recalculer ?"
