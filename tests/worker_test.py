"""
Pas un test unitaire, c'est pourquoi je ne l'ai pas nommé en test_worker.
Ce script sert au test manuel en local.
- Installer les dépandances : `poetry install -E api`
- Monter les données : `sshfs ysabell:/data/private-data/output /mnt/data-out`
- Lancer redis-server avec docker : `docker run -d -p 6379:6379 --name redis redis:alpine`
- Lancer un worker : `poetry run python redis_worker.py --id 1`
- Lancer le test : `poetry run python tests/worker_test.py`
"""

import json

from leximpact_common_python_libraries.cache import Cache
from leximpact_common_python_libraries.config import Configuration

redis_bus = Cache(project_folder="leximpact-socio-fiscal-simu-etat")
config = Configuration(project_folder="leximpact-socio-fiscal-simu-etat")
REDIS_QUEUE_NAME = config.get("REDIS_QUEUE_NAME", fail_on_missing=False, default="dev")

if __name__ == "__main__":
    json_str = """
{
  "amendement": {
    "impot_revenu.bareme_ir_depuis_1945.bareme": {
      "scale": [
        {
          "rate": {
            "value": 0
          },
          "threshold": {
            "value": 0
          }
        },
        {
          "rate": {
            "value": 0.12
          },
          "threshold": {
            "value": 11294
          }
        },
        {
          "rate": {
            "value": 0.32
          },
          "threshold": {
            "value": 28797
          }
        },
        {
          "rate": {
            "value": 0.41
          },
          "threshold": {
            "value": 82341
          }
        },
        {
          "rate": {
            "value": 0.45
          },
          "threshold": {
            "value": 177106
          }
        }
      ],
      "start": "2024-01-01",
      "type": "scale",
      "variable": "irpp_economique"
    }
  },
  "base": 2024,
  "displayMode": {
    "budget": true,
    "parametersVariableName": "csg_retraite",
    "testCasesIndex": [
      0
    ],
    "waterfallName": "brut_to_disponible"
  },
  "metadata": {
    "currency": "€",
    "packages": [
      {
        "name": "openfisca_france_with_indirect_taxation",
        "repository_url": "https://git.leximpact.dev/leximpact/simulateur-socio-fiscal/openfisca/openfisca-france-with-indirect-taxation",
        "tax_benefit_system": true,
        "version": "0.1.0"
      }
    ],
    "reforms": []
  },
  "output_variables": [
    "rfr_par_part",
    "csg_deductible_retraite",
    "csg_imposable_retraite"
  ],
  "quantile_base_variable": [
    "rfr_par_part"
  ],
  "quantile_compare_variables": [
    "csg_deductible_retraite",
    "csg_imposable_retraite",
    "rfr"
  ],
  "winners_loosers_variable": "csg_retraite",
  "quantile_nb": 10
}

"""
    msg = {"jobid": "test_local", "reform": json_str}
    redis_bus.cache.rpush(REDIS_QUEUE_NAME, json.dumps(msg))
    redis_bus.cache.expire(REDIS_QUEUE_NAME, 60)
